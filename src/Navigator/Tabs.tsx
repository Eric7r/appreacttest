import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { FibonacciScreen } from '../Screens/FibonacciScreen';
import { PalindromosScreen } from '../Screens/PalindromosScreen';
import { AtmScreen } from '../Screens/AtmScreen';

const Tab = createBottomTabNavigator();

export const Tabs =() => {
  return (
    <Tab.Navigator
     screenOptions={{
        tabBarActiveTintColor: 'green',
        tabBarStyle: {
           borderTopColor:'green',
           borderTopWidth:0
          },
          tabBarLabelStyle:{
              fontSize: 15
          }

          
     }}

     
    >
     
      <Tab.Screen name="FibonacciScreen" options={{title:'Fibonacci'}} component={FibonacciScreen} />
      <Tab.Screen name="PalindromosScreen" options={{title:'Palindromos Primos'}} component={PalindromosScreen} />
      <Tab.Screen name="AtmScreen" options={{title:'Cajero'}} component={AtmScreen} />
    </Tab.Navigator>
  );
}