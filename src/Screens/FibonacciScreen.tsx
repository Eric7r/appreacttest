import React, { useState } from 'react'
import { SafeAreaView, Button, StyleSheet, TextInput, Alert, Text } from 'react-native';


export const FibonacciScreen = () => {
    
    
    const [n, onChangeText] = useState("");
   
    const calcFibNumber = (n) => {
        
      const value: any = +n
      const prevNum: any = Math.round(value / ((1 + Math.sqrt(5)) / 2))
      const nextNum : any= value + prevNum
    

      Alert.alert(prevNum,value,nextNum);
      }


    return (
        <SafeAreaView style={styles.container}>
        <Text >Ingresa numero:</Text>
        <TextInput
          keyboardType='numeric'
          placeholder='Ingrese un numero'
          style={styles.input}
          onChangeText={onChangeText}
          value={n}
        />
        <Button
          title="Enviar"
          onPress={() => calcFibNumber(n)}
      />
       
      </SafeAreaView>
    );
}


const styles = StyleSheet.create({
    input: {
      height: 40,
      margin: 12,
      borderWidth: 1,
      padding: 10,
    },
    container : {
        flex: 1,
        justifyContent : 'center',
        
    }
  });
