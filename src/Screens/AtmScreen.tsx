import React, { useState } from 'react'
import { SafeAreaView, Button , StyleSheet, TextInput, Alert } from "react-native";

export const AtmScreen = () => {

    const [cash, onChangeText] = useState("");
    const [prevNum, setPrevNum] = useState(0);
    const [nextNum, setNextNum] = useState(0);


    const Atm = (cash) => {
      var billetes =[500,200,100,50];
      var respuesta = "";
      var res = cash;
      
      if( cash <50){
          Alert.alert("no se acepta esa denominacion")
      }
       else if ((cash % 50) != 0){
           console.log("no se puede esa cantidad");
          
      }
      else{
          for (var n of billetes) {
              var cant = res / n;
              var round= Math.floor(cant)
              res = res % n;
              respuesta += round + " billetes de " + n + " ";
              } 
              Alert.alert(respuesta);
      }
      
    }
    
    return (
        <SafeAreaView style={styles.container}>
        <TextInput
        placeholder='Ingrese un numero'
          style={styles.input}
          onChangeText={onChangeText}
          value={cash}
        />
        <Button
        title="Enviar"
        onPress={() => Atm(cash)}
      />
       
      </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    input: {
      height: 40,
      margin: 12,
      borderWidth: 1,
      padding: 10,
    },
    container : {
        flex: 1,
        justifyContent : 'center',
        
    }
  });
