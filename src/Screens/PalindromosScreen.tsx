import React from 'react';
import { FlatList, StyleSheet, Text, View } from 'react-native';


export const PalindromosScreen = () => {

    return (
        <View style={styles.container}>
          <FlatList
            data={[
              {key: '2'},
              {key: '3'},
              {key: '5'},
              {key: '7'},
              {key: '11'},
              {key: '101'},
              {key: '131'},
              {key: '151'},
              {key: '181'},
              {key: '191'},
              {key: '313'},
              {key: '353'},
              {key: '373'},
              {key: '383'},
              {key: '727'},
              {key: '757'},
              {key: '787'},
              {key: '797'},
              {key: '919'},
              {key: '929'},
            ]}
            renderItem={({item}) => <Text style={styles.item}>{item.key}</Text>}
          />
        </View>
      );
}



const styles = StyleSheet.create({
    container: {
     flex: 1,
     paddingTop: 22
    },
    item: {
      padding: 10,
      fontSize: 18,
      height: 44,
    },
  });